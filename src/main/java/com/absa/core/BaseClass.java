package com.absa.core;

import com.absa.entities.Enums;
import com.absa.tools.APIUtil;
import com.absa.tools.SeleniumDriver;

public class BaseClass {
    public static Enums.Environment currentEnvironment;
    public static Enums.BrowserType currentBrowser;
    public static SeleniumDriver seleniumDriver;
    public APIUtil apiUtil;
    public static String _reportDirectory;
    public static String TestName;

    public static void setReportDirectory(String dir) {
        _reportDirectory = dir;
    }

    public static String getReportDirectory() {
        return _reportDirectory;
    }
}
