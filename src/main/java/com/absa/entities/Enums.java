package com.absa.entities;

public class Enums {
    public enum Environment {
        AutomationURL("http://www.way2automation.com/angularjs-protractor/webtables/"),
        APIbaseURL("https://dog.ceo/api");

        public final String PageUrl;

        Environment(String pageUrl) {
            this.PageUrl = pageUrl;
        }
    }

    public enum BrowserType {
        CHROME
    }
}
