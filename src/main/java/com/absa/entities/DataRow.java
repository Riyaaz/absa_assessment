package com.absa.entities;

import java.util.LinkedList;

import com.google.common.base.Predicate;

public class DataRow {
    public LinkedList<DataColumn> DataColumns;

    public DataRow() {
        DataColumns = new LinkedList<>();
    }

    public String getColumnValue(String columnHeader) {
        try {
            Predicate<DataColumn> predicate = c -> c.getKey().equals(columnHeader);
            DataColumn obj = DataColumns.stream().filter(predicate).findFirst().get();
            return obj.getValue();
        } catch (Exception ex) {
            System.out.println(
                    ex.getMessage() + " - [ERROR] Could not find column - " + columnHeader + " - in table row");
            return "";
        }

    }

    public DataColumn getColumn(String columnHeader) {
        try {
            Predicate<DataColumn> predicate = c -> c.getKey().equals(columnHeader);
            DataColumn obj = DataColumns.stream().filter(predicate).findFirst().get();
            return obj;
        } catch (Exception ex) {
            System.out.println(
                    ex.getMessage() + " - [ERROR] Could not find column - " + columnHeader + " - in table row");
            return null;
        }

    }
}
