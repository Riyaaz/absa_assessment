package com.absa.testing.Web;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.absa.core.BaseClass;
import com.absa.entities.DataRow;
import com.absa.tools.Reporting;

import org.openqa.selenium.By;

public class AddUser extends BaseClass {

    public static String username = "";

    private By by_addUserTitle = By.xpath("//h3[text()='Add User']");
    private By by_firstname = By.name("FirstName");
    private By by_lastname = By.name("LastName");
    private By by_username = By.name("UserName");
    private By by_password = By.name("Password");
    private By by_role = By.name("RoleId");
    private By by_email = By.name("Email");
    private By by_mobilePhone = By.name("Mobilephone");
    private By by_saveBtn = By.xpath("//button[text()='Save']");

    private By by_usernameGrid(String usernameValue) {
        return By.xpath("//td[text()='" + usernameValue + "']");
    }

    private By by_customer(String customer) {
        return By.xpath("//label[text()='" + customer + "']/input");
    }

    public void waitForTitle() {
        assert seleniumDriver.waitForElement(by_addUserTitle);
        Reporting.stepPassed("Add User modal loaded");
    }

    public void enterDetails(List<DataRow> data) {
        LocalDateTime instance = LocalDateTime.now();
        DateTimeFormatter date = DateTimeFormatter.ofPattern("ddMMyyyyhhmm");

        String firstname = data.get(0).getColumnValue("Firstname");
        String lastname = data.get(0).getColumnValue("Lastname");
        username = data.get(0).getColumnValue("Username") + "_" + date.format(instance);
        String password = data.get(0).getColumnValue("Password");
        String customer = data.get(0).getColumnValue("Customer");
        String role = data.get(0).getColumnValue("Role");
        String email = data.get(0).getColumnValue("Email");
        String mobileNumber = data.get(0).getColumnValue("Cell");

        assert seleniumDriver.enterText(by_firstname, firstname);
        assert seleniumDriver.enterText(by_lastname, lastname);
        assert seleniumDriver.enterText(by_username, username);
        assert seleniumDriver.enterText(by_password, password);
        assert seleniumDriver.clickElement(by_customer(customer));
        assert seleniumDriver.selectFromDropdownList(by_role, role);
        assert seleniumDriver.enterText(by_email, email);
        assert seleniumDriver.enterText(by_mobilePhone, mobileNumber);

        Reporting.stepPassedWithScreenshot("Entered details successfully");
    }

    public void clickSave() {
        assert seleniumDriver.clickElement(by_saveBtn);
        Reporting.stepPassed("Clicked save button");
    }

    public void validateDataAdded() {
        seleniumDriver.waitForElement(by_usernameGrid(username));
        Reporting.stepPassedWithScreenshot("Validated " + username + " is on the grid");

    }
}
