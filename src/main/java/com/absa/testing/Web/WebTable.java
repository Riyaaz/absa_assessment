package com.absa.testing.Web;

import com.absa.core.BaseClass;
import com.absa.tools.Reporting;
import org.openqa.selenium.By;

public class WebTable extends BaseClass {

    private By by_table = By.xpath("//table[@table-title='Smart Table example']");
    private By by_btnAddUser = By.xpath("//button[text()=' Add User']");

    public void navigate(String url) {
        assert seleniumDriver.navigate(url);
    }

    public void verifyPageLoaded() {
        assert seleniumDriver.waitForElement(by_table);
        assert seleniumDriver.waitForElement(by_btnAddUser);
        Reporting.stepPassedWithScreenshot("Page loaded successfully");
    }

    public void clickAddUser() {
        assert seleniumDriver.clickElement(by_btnAddUser);
        Reporting.stepPassed("Clicked Add User button");
    }
}
