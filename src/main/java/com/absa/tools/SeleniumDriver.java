package com.absa.tools;

import java.io.File;

import com.absa.core.BaseClass;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumDriver extends BaseClass {
    private WebDriver driver;
    private static int screenshotCounter;

    public SeleniumDriver() {
        launchDriver();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public boolean launchDriver() {
        switch (currentBrowser) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--disable-gpu");
                options.addArguments("--disable-notifications");
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                capabilities.merge(options);
                this.driver = new ChromeDriver(options);
                break;
            default:
                break;
        }
        this.driver.manage().window().maximize();
        return true;
    }

    public boolean shutdown() {
        try {
            this.driver.quit();
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public boolean navigate(String url) {
        try {
            this.driver.navigate().to(url);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean waitForElement(By selector) {
        try {
            WebDriverWait wait = new WebDriverWait(this.driver, 5);
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public boolean clickElement(By selector) {
        try {
            waitForElement(selector);
            WebDriverWait wait = new WebDriverWait(this.driver, 1);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
            WebElement elementToClick = this.driver.findElement(selector);
            elementToClick.click();

            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public boolean selectFromDropdownList(By selector, String valueToSelect) {
        try {
            waitForElement(selector);
            Select dropdownList = new Select(this.driver.findElement(selector));
            WebElement formxpath = driver.findElement(selector);
            formxpath.click();
            dropdownList.selectByVisibleText(valueToSelect);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean enterText(By selector, String textToEnter) {
        try {
            waitForElement(selector);
            WebDriverWait wait = new WebDriverWait(this.driver, 1);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
            WebElement elementToClick = this.driver.findElement(selector);
            elementToClick.sendKeys(textToEnter);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String takeScreenshot(boolean status) {
        screenshotCounter++;
        StringBuilder imagePathBuilder = new StringBuilder();
        StringBuilder relativePathBuilder = new StringBuilder();
        try {
            imagePathBuilder.append(getReportDirectory());
            relativePathBuilder.append("Screenshots\\");
            new File(imagePathBuilder.toString() + (relativePathBuilder).toString()).mkdirs();

            relativePathBuilder.append(screenshotCounter + "_");
            if (status) {
                relativePathBuilder.append("PASSED");
            } else {
                relativePathBuilder.append("FAILED");
            }
            relativePathBuilder.append(".png");

            File screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File(imagePathBuilder.append(relativePathBuilder).toString()));

            return "./" + relativePathBuilder.toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }
}
