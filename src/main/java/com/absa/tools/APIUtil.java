package com.absa.tools;

import java.io.FileReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class APIUtil {
    public HttpResponse httpget(final String endpoint) {

        try {
            HttpUriRequest request = new HttpGet(endpoint);

            HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
            return httpResponse;
        } catch (Exception ex) {
            return null;
        }
    }

    public String httpStringResponse(HttpResponse httpResponse) {
        try {
            final HttpEntity entity = httpResponse.getEntity();
            final String responseString = EntityUtils.toString(entity, "UTF-8");
            return responseString;
        } catch (Exception ex) {
            return "";
        }
    }

    public JSONObject createJsonObject() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser
                    .parse(new FileReader(System.getProperty("user.dir") + "\\data\\data.json"));

            return jsonObject;
        } catch (Exception ex) {
            ex.getMessage();
            return null;
        }
    }

}
