package com.absa.tools;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.absa.core.BaseClass;
import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class Reporting extends BaseClass {

    private static ExtentReports report;
    private static ExtentTest currentTest;
    public static int ScreenShotCounter = 0;
    public static String ScreenShotName;

    private static void setup() {
        setReportDirectory(System.getProperty("user.dir") + "\\Reports\\" + getCurTime() + "\\");
        new File(getReportDirectory()).mkdirs();
        report = new ExtentReports();
        ExtentSparkReporter html = new ExtentSparkReporter(getReportDirectory() + "ExtentReport.html");
        report.attachReporter(html);
        report.setAnalysisStrategy(AnalysisStrategy.TEST);

        report.flush();
    }

    public static void setReportName() {
        setReportDirectory(System.getProperty("user.dir") + "com.itassessment\\Reports\\" + getCurTime() + "\\");
        new File(getReportDirectory()).mkdirs();
        report = new ExtentReports();
        ExtentSparkReporter html = new ExtentSparkReporter(getReportDirectory() + "ExtentReport.html");
        report.attachReporter(html);
        report.setAnalysisStrategy(AnalysisStrategy.TEST);

        report.flush();
    }

    public static void createTest() {
        try {
            if (report == null)
                setup();
            if (currentTest == null || !currentTest.getModel().getName().equals(TestName)) {
                currentTest = report.createTest(TestName);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void stepPassed(String message) {
        if (currentTest == null)
            createTest();
        currentTest.pass(message);
        System.out.println("[SUCCESS] - " + message);

        report.flush();
    }

    public static void info(String message) {
        if (currentTest == null)
            createTest();
        currentTest.info(message);
        System.out.println("[INFO] - " + message);

        report.flush();
    }

    public static void warning(String message) {
        if (currentTest == null)
            createTest();
        currentTest.warning(message);
        System.out.println("[WARN] - " + message);

        report.flush();
    }

    public static void stepPassedWithScreenshot(String message) {
        if (currentTest == null)
            createTest();
        try {
            MediaEntityBuilder med;

            med = MediaEntityBuilder.createScreenCaptureFromPath(seleniumDriver.takeScreenshot(true));

            currentTest.pass(message, med.build());

        } catch (Exception e) {
            currentTest.pass(message + " - screenshot capture failure");
        }

        System.out.println(message);
        report.flush();
    }

    public static String testFailed(String message) {
        if (currentTest == null)
            createTest();
        try {
            MediaEntityBuilder med;

            med = MediaEntityBuilder.createScreenCaptureFromPath(seleniumDriver.takeScreenshot(true));

            currentTest.fail(message, med.build());
        } catch (Exception ex) {
            currentTest.fail(message + " - screenshot capture failure");
        }
        report.flush();
        return message;
    }

    private static String getCurTime() {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");

        return ft.format(date);
    }
}
