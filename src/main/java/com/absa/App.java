package com.absa;

import com.absa.core.BaseClass;
import com.absa.testing.Web.AddUser;
import com.absa.testing.Web.WebTable;
import com.absa.tools.APIUtil;
import com.absa.tools.SeleniumDriver;

public class App extends BaseClass {
    WebTable webTable;
    AddUser addUser;

    public App(String type) {
        webTable = new WebTable();
        addUser = new AddUser();
        if (type.equalsIgnoreCase("Web")) {
            seleniumDriver = new SeleniumDriver();
        } else {
            apiUtil = new APIUtil();
        }
    }
}
