package com.absa;

import java.util.List;
import com.absa.core.BaseClass;
import com.absa.entities.DataRow;
import com.absa.entities.Enums.BrowserType;
import com.absa.entities.Enums.Environment;
import com.absa.tools.ExcelReader;
import com.absa.tools.Reporting;

import org.junit.After;
import org.junit.Test;

public class WebTests extends BaseClass {

    @After
    public void cleanUp() {
        seleniumDriver.shutdown();
    }

    @Test
    public void Test1() {

        TestName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        Reporting.createTest();
        String dataPath = System.getProperty("user.dir") + "\\data\\absa.xlsx";
        List<DataRow> data = ExcelReader.GetDataSet(dataPath, "Testcase1");

        currentEnvironment = Environment.AutomationURL;
        currentBrowser = BrowserType.CHROME;
        App app = new App("Web");

        app.webTable.navigate(currentEnvironment.PageUrl);
        app.webTable.verifyPageLoaded();
        app.webTable.clickAddUser();
        app.addUser.waitForTitle();
        app.addUser.enterDetails(data);
        app.addUser.clickSave();
        app.addUser.validateDataAdded();
    }

    @Test
    public void Test2() {
        TestName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        Reporting.createTest();
        String dataPath = System.getProperty("user.dir") + "\\data\\absa.xlsx";
        List<DataRow> data = ExcelReader.GetDataSet(dataPath, "Testcase2");

        currentEnvironment = Environment.AutomationURL;
        currentBrowser = BrowserType.CHROME;
        App app = new App("Web");

        app.webTable.navigate(currentEnvironment.PageUrl);
        app.webTable.verifyPageLoaded();
        app.webTable.clickAddUser();
        app.addUser.waitForTitle();
        app.addUser.enterDetails(data);
        app.addUser.clickSave();
        app.addUser.validateDataAdded();
    }
}
