package com.absa;

import java.io.IOException;
import com.absa.core.BaseClass;
import com.absa.entities.Enums;
import com.absa.tools.Reporting;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpResponseException;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.simple.JSONObject;
import org.junit.Test;

public class APItests extends BaseClass {

    @Test
    public void getAllBreeds() throws HttpResponseException, IOException {
        TestName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        Reporting.createTest();
        App app = new App("API");
        int code = app.apiUtil.httpget(Enums.Environment.APIbaseURL.PageUrl + "/breeds/list/all").getStatusLine()
                .getStatusCode();

        HttpResponse response = app.apiUtil.httpget(Enums.Environment.APIbaseURL.PageUrl + "/breeds/list/all");
        String responseString = new BasicResponseHandler().handleResponse(response);

        if (HttpStatus.SC_OK == code) {
            Reporting.stepPassed("Response: " + responseString);
            Reporting.stepPassed("Status Code: " + code);
        } else {
            Reporting.testFailed("Expected: " + HttpStatus.SC_OK + " | " + "Actual: " + code);
        }

    }

    @Test
    public void verifyRetrieverBreed() throws HttpResponseException, IOException, InterruptedException {
        TestName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        Reporting.createTest();
        App app = new App("API");
        JSONObject data = app.apiUtil.createJsonObject();
        String breed = (String) data.get("Breed");
        System.out.println(breed);
        HttpResponse response = app.apiUtil.httpget(Enums.Environment.APIbaseURL.PageUrl + "/breeds/list/all");
        String responseString = new BasicResponseHandler().handleResponse(response);

        if (responseString.contains(breed)) {
            Reporting.stepPassed("Response: " + responseString);
        } else {
            Reporting.testFailed("Response does not contain " + breed);
        }

    }

    @Test
    public void validateSubBreedsForRetriever() throws HttpResponseException, IOException {
        TestName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        Reporting.createTest();
        App app = new App("API");
        int code = app.apiUtil.httpget(Enums.Environment.APIbaseURL.PageUrl + "/breed/retriever/list").getStatusLine()
                .getStatusCode();
        HttpResponse response = app.apiUtil.httpget(Enums.Environment.APIbaseURL.PageUrl + "/breed/retriever/list");
        String responseString = new BasicResponseHandler().handleResponse(response);

        if (HttpStatus.SC_OK == code) {
            Reporting.stepPassed("Response: " + responseString);
            Reporting.stepPassed("Status Code: " + code);
        } else {
            Reporting.testFailed("Expected: " + HttpStatus.SC_OK + " | " + "Actual: " + code);
        }

    }

    @Test
    public void validaterandomImage() throws HttpResponseException, IOException {
        TestName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        Reporting.createTest();
        App app = new App("API");
        JSONObject data = app.apiUtil.createJsonObject();
        String breed = (String) data.get("Breed");
        String subBreed = (String) data.get("Sub-breed");

        String endPoint = Enums.Environment.APIbaseURL.PageUrl + "/breed/" + breed + "/" + subBreed + "/images/random";
        int code = app.apiUtil.httpget(endPoint).getStatusLine().getStatusCode();

        HttpResponse response = app.apiUtil.httpget(endPoint);
        String responseString = new BasicResponseHandler().handleResponse(response);

        if (HttpStatus.SC_OK == code) {
            Reporting.stepPassed("Response: " + responseString);
            Reporting.stepPassed("Status Code: " + code);
        } else {
            Reporting.testFailed("Expected: " + HttpStatus.SC_OK + " | " + "Actual: " + code);
        }

    }

}
