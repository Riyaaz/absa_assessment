### List of tools used

#### 1. Open JDK

#### 2. Visual Studio Code

#### 3. Junit 4

#### 4. Json.simple

#### 5. Apache.http

#### 6. Extent Reports

#### A JDK needs to be installed and a JAVA_HOME system environment variable needs to be set.

#### In the POM File in the properties section -> the maven compiler source and target needs to be changed according to your jdk. e.g I'm using JDK version 14 as shown in the pom file.

#### I tested the web tests on chrome so it is required for chrome to be installed.

### Executing tests

#### 1. Execute tests through cmd using maven by open the cmd in the project root folder com.itassessment

#### 2. Maven has to be installed on the pc and should be part of the system environment variables.

#### 3. There are 2 test suites -> WebTests.java and APItests.java and it contains unit tests within them.

#### 4. Use the following command as an example -> "mvn -Dtest=APItests#verifyRetrieverBreed test" to execute the unit tests within the test suite.

#### 5. To execute the entire testsuite, use the following command as an example -> "mvn -Dtest=APItests.java test"

## Test Suites and unit tests available are

### - WebTests.java

#### - Test1

#### - Test2

### - APITests.java

#### - getAllBreeds

#### - verifyRetrieverBreed

#### - validateSubBreedsForRetriever

#### - validaterandomImage
